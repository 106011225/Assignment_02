# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description
at menu:
    change volume, difficulty
    show leaderboard (firebase)
    bgm

when playing:
    press 'p' to pause game
    score, lives, ultimate skill number on the top 
    bgm and shooting sound
    moving bg
    animation on player(fire)
    normal enemies shoot bullets(auto aiming)
    bullet and enemies damage you
    bullet particle system
    press 'z' to attack, 'x' to use ultimate
    ultimate attack damage enemies and bullets around you
    kill enemies or collect item to get scores
    after killing enemies, possibly generate items
    items:
	heart: add lives
	shield: safety mask (little helper)
	green box: unique bullet
	radiation icon: add one ultimate
    boss appears after 10s
    boss hits you every some time interval
    kill boss to get scores
after lives = 0:
    if breaking the recrd, input your name and refresh leaderboard
    play again or goto menu
    

# Basic Components Description : 
1. Jucify mechanisms : [select difficulty at menu, press 'x' to use ultimate skill]
2. Animations : [animation on fire]
3. Particle Systems : [particle system on bullets]
4. Sound effects : [bgm, shooting sound; change volume at menu]
5. Leaderboard : [input your name when you break the record]

# Bonus Functions Description : 
1. [flash] : [add flash effect when player is damaged]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]

