var gameoverState = {

    create: function() {
        
        game.add.image(0, 0, 'background');

        var scoreLabel = game.add.text(game.width/2, game.height/2,
            'your score: ' + game.global.lastScore, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);

        var scoreLabel = game.add.text(game.width/2, game.height/2 + 30,
            'highest score: ' + game.global.highestScore + ' by ' + game.global.name,
            { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);


        var Label1 = game.add.text(game.width/2, game.height-80,
            'press Z to go to menu', { font: '25px Arial', fill: '#ffffff' });
        Label1.anchor.setTo(0.5, 0.5);

        var Label2 = game.add.text(game.width/2, game.height-160,
            'press X to play again', { font: '25px Arial', fill: '#ffffff' });
        Label2.anchor.setTo(0.5, 0.5);
        
        
        var keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        keyZ.onDown.add(this.gomenu, this);

        var keyX = game.input.keyboard.addKey(Phaser.Keyboard.X);
        keyX.onDown.add(this.playagain, this);

    },
    gomenu: function(){
        game.state.start('menu');
    },
    playagain: function(){
        game.state.start('play');
    }

};



