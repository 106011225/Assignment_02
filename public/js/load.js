var loadState = {

    preload: function() {
        
        
        
        //text 
        var loadingLabel = game.add.text(game.width/2, 150, 
            'loading...', {font:'30px Arial', fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);
        
        //progress bar 
        this.progressBar = game.add.sprite(50, 200, 'progressBar');
        this.progressBar.anchor.setTo(0,0);
        this.progressBar.scale.setTo(1,1);
        this.game.load.setPreloadSprite(this.progressBar);

        //load assets
        game.load.spritesheet('airplane', 'assets/airplane.png',72,72);
        game.load.image('bulletA1', 'assets/bulletA1.png');
        game.load.image('bulletA3', 'assets/bulletA3.png');
        game.load.image('item1', 'assets/item1.png');
        game.load.image('enemy1', 'assets/enemy1.png');
        game.load.image('bulletE1', 'assets/bulletE1.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('shield', 'assets/shield.png');
        game.load.image('mask', 'assets/mask.png');
        game.load.image('heart', 'assets/heart.png');
        game.load.image('radiation', 'assets/radiation.png');
        game.load.image('boss', 'assets/boss.png');
        game.load.image('explosionPic', 'assets/explosionPic.png');

        game.load.audio('AnotherMedium','assets/AnotherMedium.ogg');
        game.load.audio('GunSound','assets/GunSound.ogg');
        game.load.audio('menuMusic', 'assets/sans.ogg');

        //load menu background
        game.load.image('background', 'assets/background.jpg');


    },
    create: function() {
        //fetch leaderboard data 
        firebase.database().ref().once('value').then(function(snapshot){
            game.global.name = snapshot.val().name;
            game.global.highestScore = snapshot.val().score;
            
            
            game.state.start('menu');
        
        
        }).catch(e => console.log(e.message));
    }

};


