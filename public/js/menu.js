var menuState = {

    create: function() {
        
        game.add.image(0, 0, 'background');

        //background music
        this.backgroundMusic = game.add.audio('menuMusic');
        this.backgroundMusic.volume = game.global.volume / 10;
        this.backgroundMusic.loop = true;
        this.backgroundMusic.play();

        // highest score 
        var scoreLabel = game.add.text(game.width/2, game.height/2,
            'highest score: ' + game.global.highestScore + ' by ' + game.global.name,
            { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);

        //start game 
        var startLabel = game.add.text(game.width/2, game.height-80,
            'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);

        //difficulty 
        var setEazyLabel = game.add.text(game.width/2, game.height-160,
            'press E to set difficulty EAZY', { font: '25px Arial', fill: '#ffffff' });
        setEazyLabel.anchor.setTo(0.5, 0.5);
        var Ekey = game.input.keyboard.addKey(Phaser.Keyboard.E);
        Ekey.onDown.add(this.setEazy, this);
        var setHardLabel = game.add.text(game.width/2, game.height-190,
            'press H to set difficulty HARD', { font: '25px Arial', fill: '#ffffff' });
        setHardLabel.anchor.setTo(0.5, 0.5);
        var Hkey = game.input.keyboard.addKey(Phaser.Keyboard.H);
        Hkey.onDown.add(this.setHard, this);

        this.diffLabel = game.add.text(game.width/2, game.height/2-50,
            'difficulty: ' + ((game.global.difficulty==0)?('EAZY'):('HARD')), { font: '25px Arial', fill: '#ffffff' });
        this.diffLabel.anchor.setTo(0.5, 0.5);



        //volume 
        var incVLabel = game.add.text(game.width/2, game.height-220,
            'press I to increase volume', { font: '25px Arial', fill: '#ffffff' });
        incVLabel.anchor.setTo(0.5, 0.5);
        var Ikey = game.input.keyboard.addKey(Phaser.Keyboard.I);
        Ikey.onDown.add(this.incVolume, this);
        var decVLabel = game.add.text(game.width/2, game.height-250,
            'press D to decrease volume', { font: '25px Arial', fill: '#ffffff' });
        decVLabel.anchor.setTo(0.5, 0.5);
        var Dkey = game.input.keyboard.addKey(Phaser.Keyboard.D);
        Dkey.onDown.add(this.decVolume, this);

        this.volLabel = game.add.text(game.width/2, game.height/2-100,
            'volume: ' + game.global.volume, { font: '25px Arial', fill: '#ffffff' });
        this.volLabel.anchor.setTo(0.5, 0.5);

    },
    start: function(){
        this.backgroundMusic.pause();
        game.state.start('play');
    },
    setEazy: function(){
        game.global.difficulty = 0;
        this.diffLabel.text = 'difficulty: ' + ((game.global.difficulty==0)?('EAZY'):('HARD'));
    },
    setHard: function(){
        game.global.difficulty = 1;
        this.diffLabel.text = 'difficulty: ' + ((game.global.difficulty==0)?('EAZY'):('HARD'));
    },
    incVolume: function(){
        if (game.global.volume < 10) game.global.volume += 1;
        this.volLabel.text = 'volume: ' + game.global.volume;
        this.backgroundMusic.volume = game.global.volume / 10;
    },
    decVolume: function(){
        if (game.global.volume > 0) game.global.volume -= 1;
        this.volLabel.text = 'volume: ' + game.global.volume;
        this.backgroundMusic.volume = game.global.volume / 10;
    }

};



