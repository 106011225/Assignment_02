var background;
var backgroundMusic;
var gunSound;

var score = 0;

var ulting = false;

var aimOn = false;
var shieldOn = false;

var bossTween1;

var bossOnPlayer = false;

var playState = {
    
    preload: function(){},
    create: function(){
        
        background = game.add.tileSprite(0,0,game.global.gameWidth,game.global.gameHeight,'background');
        backgroundMusic = game.add.audio('AnotherMedium');
        backgroundMusic.loop = true;
        backgroundMusic.volume = game.global.volume / 10;
        backgroundMusic.play();

        gunSound = game.add.audio('GunSound');
        gunSound.volume = game.global.volume/50;

        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //keyboard 
        this.cursor = game.input.keyboard.createCursorKeys();
        this.zxp = {
            z: game.input.keyboard.addKey(Phaser.Keyboard.Z),
            x: game.input.keyboard.addKey(Phaser.Keyboard.X),
            p: game.input.keyboard.addKey(Phaser.Keyboard.P)
        };
        
        //player(airplane)
        this.player = game.add.sprite(game.width/2, game.height/5*4, 'airplane');
        this.player.anchor.setTo(0.5,0.5);
        this.player.scale.setTo(0.75,0.75);
        this.player.animations.add('flying',[0,3],60,true);
        game.physics.arcade.enable(this.player);
        this.player.shootRate = 150;
        this.player.shootTime = this.time.now;
        this.player.lives = 10;
        this.player.ult = 5;
        this.player.ultRate = 500;
        this.player.ultTime = this.time.now;

        //display remaining lives
        this.livesLabel = game.add.text(10,30,'lives: '+this.player.lives,
            {font:'18px Arial', fill:'#ffffff'});
        this.livesLabel.anchor.setTo(0,0);

        //display score  
        score = 0;
        this.scoreLabel = game.add.text(10, 10,
            'score: ' + score, { font: '18px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0, 0);
        //display ult 
        this.ultLabel = game.add.text(10,50,'ultimate: '+this.player.ult,
            {font:'18px Arial', fill:'#ffffff'});
        this.ultLabel.anchor.setTo(0,0);


        //bullet 
        this.Abullets = game.add.group();
        this.Abullets.enableBody = true;
        this.Abullets.createMultiple(40, 'bulletA1');
        this.Abullets3 = game.add.group();
        this.Abullets3.enableBody = true;
        this.Abullets3.createMultiple(40, 'bulletA3');
        this.Ebullets = game.add.group();
        this.Ebullets.enableBody = true;
        this.Ebullets.createMultiple(40, 'bulletE1');

        //item
        this.item1 = game.add.group();
        this.item1.enableBody = true;
        this.item1.createMultiple(1, 'item1');
        
        //shield item 
        this.shield = game.add.group();
        this.shield.enableBody = true;
        this.shield.createMultiple(1, 'shield');

        //mask 
        this.mask = game.add.sprite(0,0,'mask');
        this.mask.anchor.setTo(0.5,0.5);
        this.mask.scale.setTo(0.2,0.2);
        this.mask.alpha = 0;

        //heart item
        this.heart = game.add.group();
        this.heart.enableBody = true;
        this.heart.createMultiple(1, 'heart');
        //radiation item
        this.radiation = game.add.group();
        this.radiation.enableBody = true;
        this.radiation.createMultiple(1, 'radiation');
        

        //partical
        this.emitter = game.add.emitter(0,0,15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150,150);
        this.emitter.setXSpeed(-150,150);
        this.emitter.setScale(2,0,2,0,800);
        this.emitter.gravity = 0;

        //enemy 
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        if (game.global.difficulty == 0) this.enemies.createMultiple(4, 'enemy1');
        else this.enemies.createMultiple(7, 'enemy1');
        
        //boss
        this.boss = game.add.group();
        this.boss.enableBody = true;
        this.boss.createMultiple(1, 'boss');
        
        //enemy generate
        if (game.global.difficulty == 0) game.time.events.loop(2000,this.addEnemy, this);
        else game.time.events.loop(1000,this.addEnemy, this);

        //boss generate
        game.time.events.loop(10000,this.addBoss, this);

    },
    update: function(){
       
        this.gamePauseDetect();

        background.tilePosition.y += 1;

        this.player.animations.play('flying');
        this.movePlayer();
        this.addAbullets();
        this.ultimateSkill();
        this.updateMaskStatus();

        this.enemiesShoot();

        game.physics.arcade.overlap(this.Abullets, this.enemies, this.hitEnemyA, null, this);
        game.physics.arcade.overlap(this.Abullets3, this.enemies, this.hitEnemyA, null, this);
        game.physics.arcade.overlap(this.Abullets, this.boss, this.hitBoss, null, this);
        game.physics.arcade.overlap(this.Abullets3, this.boss, this.hitBoss, null, this);
        game.physics.arcade.overlap(this.player, this.enemies, this.hitPlayer, null, this);
        game.physics.arcade.overlap(this.player, this.boss, this.hitPlayer, null, this);
        game.physics.arcade.overlap(this.player, this.Ebullets, this.hitPlayer, null, this);
        game.physics.arcade.overlap(this.player, this.item1, this.setAimOn, null, this);
        game.physics.arcade.overlap(this.player, this.shield, this.setShieldOn, null, this);
        game.physics.arcade.overlap(this.player, this.heart, this.addlives, null, this);
        game.physics.arcade.overlap(this.player, this.radiation, this.addult, null, this);

        if (ulting) this.ultKill();

    },
    movePlayer: function(){
        if(this.player.body.x >= -1*this.player.width/2 && this.cursor.left.isDown && this.cursor.right.isUp){
            this.player.body.velocity.x = -300;
        }
        else if (this.player.body.x <= game.width-this.player.width && this.cursor.right.isDown && this.cursor.left.isUp){
            this.player.body.velocity.x = 300;
        }
        else {
            this.player.body.velocity.x = 0;
        }

        if(this.player.body.y >= -1*this.player.height/2 && this.cursor.up.isDown && this.cursor.down.isUp){
            this.player.body.velocity.y = -300;
        }
        else if (this.player.body.y <= game.height-this.player.height/2 && this.cursor.down.isDown && this.cursor.up.isUp){
            this.player.body.velocity.y = 300;
        }
        else {
            this.player.body.velocity.y = 0;
        }
    },
    addAbullets: function(){
        if(this.zxp.z.isDown && this.time.now-this.player.shootTime >= this.player.shootRate){
            this.player.shootTime = this.time.now;
            
            //basic bullet
            var bullet = this.Abullets.getFirstDead();
            if(!bullet){return;}
            bullet.anchor.setTo(0.5,1);
            bullet.reset(this.player.x, this.player.y-this.player.height/2);
            bullet.body.velocity.y = -1000;
            bullet.checkWorldBounds = true;
            bullet.outOfBoundsKill = true;


            //aiming bullet 
            if (aimOn == true){
                var nearestEnemy, Dist=100000, length;
                this.enemies.forEach(function(enemy){
                    if(enemy.alive ){
                        length = Math.pow(Math.pow(enemy.x-this.player.x,2) + Math.pow(enemy.y-this.player.y,2),0.5);
                        if (length < Dist){
                            nearestEnemy = enemy;
                            Dist = length; 
                        }
                    }
                },this);
                this.boss.forEach(function(enemy){
                    if(enemy.alive ){
                        length = Math.pow(Math.pow(enemy.x-this.player.x,2) + Math.pow(enemy.y-this.player.y,2),0.5);
                        if (length < Dist){
                            nearestEnemy = enemy;
                            Dist = length; 
                        }
                    }
                },this);

                if (Dist != 100000){
                    var bullet3 = this.Abullets3.getFirstDead();
                    if(!bullet3){return;}
                    bullet3.anchor.setTo(0.5,1);
                    bullet3.reset(this.player.x, this.player.y-this.player.height/2);
                    bullet3.body.velocity.x = (nearestEnemy.x - this.player.x) / Dist * 800;
                    bullet3.body.velocity.y = (nearestEnemy.y - this.player.y) / Dist * 800;
                    bullet3.checkWorldBounds = true;
                    bullet3.outOfBoundsKill = true;
                }
            }

            //play sound effect
            gunSound.play();
        }
    },
    setAimOn: function(player,item1){
        score += 50;
        this.scoreLabel.text = 'score: ' + score;
        item1.kill();
        aimOn = true;

    },
    setShieldOn: function(player,shield){
        score += 50;
        this.scoreLabel.text = 'score: ' + score;
        shield.kill();
        shieldOn = true;
    },
    addlives: function(player,heart){
        this.player.lives += 1;
        this.livesLabel.text = 'lives: ' + this.player.lives;
        heart.kill();
    },
    addult: function(player,radiation){
        this.player.ult += 1;
        this.ultLabel.text = 'ultimate: ' + this.player.ult;
        radiation.kill();

    },
    updateMaskStatus: function(){
        if (shieldOn) this.mask.alpha = 1;
        else this.mask.alpha = 0;

        this.mask.x = this.player.x;
        this.mask.y = this.player.y;
    },
    ultimateSkill: function(){
        
        if (this.zxp.x.isDown && this.player.ult>0 && this.time.now-this.player.ultTime >= this.player.ultRate){
            
            this.player.ult -= 1;
            this.ultLabel.text = 'ultimate: ' + this.player.ult;

            ulting = true;

            this.player.ultTime = this.time.now;
            game.camera.shake(0.005,200);
            
            this.explosionPic = game.add.sprite(this.player.x, this.player.y, 'explosionPic');
            this.explosionPic.anchor.setTo(0.5,0.5);
            this.explosionPic.scale.setTo(0,0);
            this.explosionPic.alpha = 1;
            game.add.tween(this.explosionPic).to({alpha:0.1},450).start();
            var tween = game.add.tween(this.explosionPic.scale).to({x:0.8,y:0.8},450).start();
           
            tween.onComplete.add(function(){
                ulting=false; 
                this.explosionPic.destroy();
            },this);
        }
    },
    addEnemy: function(){
        var enemy = this.enemies.getFirstDead();
        if(!enemy){return;}
        enemy.anchor.setTo(0.5,0.5);
        var x;
        x = game.rnd.integerInRange(0,game.global.gameWidth);
        enemy.reset(x, 0);
        enemy.scale.setTo(0.7,0.7)
        enemy.body.velocity.x = game.rnd.integerInRange(-150,150);
        enemy.body.velocity.y = game.rnd.integerInRange(30,100);
        enemy.body.bounce.x = 1;
        enemy.body.bounce.y = 1;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        enemy.body.collideWorldBounds = true;
        enemy.health = 5;
        enemy.shootTime = this.time.now;
        if(game.global.difficulty == 0)enemy.shootRate = 1500;
        else enemy.shootRate = 1500;
        enemy.type = 'enemy';
    },
    addBoss: function(){
        var boss = this.boss.getFirstDead();
        if(!boss){return;}
        boss.anchor.setTo(0.7,0.7);
        
        boss.reset(game.global.gameWidth/2, 0);
        boss.scale.setTo(0.5,0.5);

        bossTween3 = game.add.tween(boss.body).to({y:100},300).start();
        bossTween2 = bossTween3;
        bossTween1 = game.time.events.loop(10000,function(){
            var tx = this.player.x, ty = this.player.y;
            if (tx < 50) tx = 50;
            else if (tx > game.global.gameWidth-50) tx = game.global.gameWidth - 50;
            if (ty < 50) ty = 50;
            else if (ty > game.global.gameHeight-50) ty = game.global.gameHeight - 50;
            bossTween2 = game.add.tween(boss.body).to({x:tx, y:ty},500).yoyo(true).start();
        }, this);

        boss.checkWorldBounds = true;
        boss.outOfBoundsKill = true;
        boss.body.collideWorldBounds = true;
        boss.health = 20;
        boss.shootTime = this.time.now;
        boss.shootRate = 1000;
        boss.type = 'boss';
        
    },
    hitBoss: function(bullet,boss){
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true,800,null,15);

        bullet.kill();
        
        boss.damage(1);
        if (boss.health <= 0) {
            score += 300;
            this.scoreLabel.text = 'score: ' + score;
            
            game.time.events.remove(bossTween1);
            bossTween2.stop();
            bossTween3.stop();

        }

    },
    hitEnemyA: function(bullet,enemy){
        
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true,800,null,15);

        bullet.kill();
        enemy.damage(1);
        if (enemy.health <= 0) {
            score += 100;
            this.scoreLabel.text = 'score: ' + score;
            
            //generate item1 
            var tmp = game.rnd.integerInRange(1,4);
            if (tmp == 1){
                var item = this.item1.getFirstDead();
                if(!item){return;}
                item.anchor.setTo(0.5,0.5);
                item.scale.setTo(0.3,0.3);
                item.reset(enemy.x,enemy.y);
                var v = [-300,300];
                item.body.velocity.x = game.rnd.pick(v);
                item.body.velocity.y = game.rnd.pick(v);
                item.body.bounce.x = 1;
                item.body.bounce.y = 1;
                item.checkWorldBounds = true;
                item.outOfBoundsKill = true;
                item.body.collideWorldBounds = true;
                item.lifespan = 5000;
            }
            //generate shield item  
            var tmp2 = game.rnd.integerInRange(1,4);
            if (tmp2 == 1){
                var item = this.shield.getFirstDead();
                if(!item){return;}
                item.anchor.setTo(0.5,0.5);
                item.scale.setTo(0.07,0.07);
                item.reset(enemy.x,enemy.y);
                var v = [-300,300];
                item.body.velocity.x = game.rnd.pick(v);
                item.body.velocity.y = game.rnd.pick(v);
                item.body.bounce.x = 1;
                item.body.bounce.y = 1;
                item.checkWorldBounds = true;
                item.outOfBoundsKill = true;
                item.body.collideWorldBounds = true;
                item.lifespan = 6000;
            }
            //generate heart item  
            var tmp3 = game.rnd.integerInRange(1,4);
            if (tmp3 == 1){
                var item = this.heart.getFirstDead();
                if(!item){return;}
                item.anchor.setTo(0.5,0.5);
                item.scale.setTo(0.03,0.03);
                item.reset(enemy.x,enemy.y);
                var v = [-300,300];
                item.body.velocity.x = game.rnd.pick(v);
                item.body.velocity.y = game.rnd.pick(v);
                item.body.bounce.x = 1;
                item.body.bounce.y = 1;
                item.checkWorldBounds = true;
                item.outOfBoundsKill = true;
                item.body.collideWorldBounds = true;
                item.lifespan = 6000;
            }
            //generate radiation item  
            var tmp3 = game.rnd.integerInRange(1,4);
            if (tmp3 == 1){
                var item = this.radiation.getFirstDead();
                if(!item){return;}
                item.anchor.setTo(0.5,0.5);
                item.scale.setTo(0.06,0.06);
                item.reset(enemy.x,enemy.y);
                var v = [-300,300];
                item.body.velocity.x = game.rnd.pick(v);
                item.body.velocity.y = game.rnd.pick(v);
                item.body.bounce.x = 1;
                item.body.bounce.y = 1;
                item.checkWorldBounds = true;
                item.outOfBoundsKill = true;
                item.body.collideWorldBounds = true;
                item.lifespan = 6000;
            }

        }
    },
    enemiesShoot: function(){
        this.enemies.forEach(function(enemy){
            if(enemy.alive){
                if (this.time.now - enemy.shootTime >= enemy.shootRate){
                    enemy.shootTime = this.time.now;

                    var bullet = this.Ebullets.getFirstDead();
                    if(!bullet){return;}
                    bullet.anchor.setTo(0.5,0.5);
                    bullet.reset(enemy.x, enemy.y);
                    var distance = Math.pow(Math.pow(this.player.x - enemy.x,2)+Math.pow(this.player.y - enemy.y,2),0.5)
                    bullet.body.velocity.y = (this.player.y - enemy.y)/distance*200;
                    bullet.body.velocity.x = (this.player.x - enemy.x)/distance*200;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                }
            }
        },this);

        this.boss.forEach(function(enemy){
            if(enemy.alive){
                if (this.time.now - enemy.shootTime >= enemy.shootRate){
                    enemy.shootTime = this.time.now;

                    var bullet = this.Ebullets.getFirstDead();
                    if(!bullet){return;}
                    bullet.anchor.setTo(0.5,0.5);
                    bullet.reset(enemy.x, enemy.y);
                    var distance = Math.pow(Math.pow(this.player.x - enemy.x,2)+Math.pow(this.player.y - enemy.y,2),0.5)
                    bullet.body.velocity.y = (this.player.y - enemy.y)/distance*200;
                    bullet.body.velocity.x = (this.player.x - enemy.x)/distance*200;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                }
            }
        },this);
    },
    hitPlayer: function(player,object){
        
        if (object.type != 'boss') object.kill();
        if (object.type == 'boss' && bossOnPlayer==true){return;}
        else if (object.type == 'boss') {
            bossOnPlayer = true;
            game.time.events.add(2000,function(){bossOnPlayer=false;});
        }
        if (shieldOn == false){
            this.player.lives -= 1;
            game.camera.flash(0xff0000,200)
            this.livesLabel.text = 'lives: ' + this.player.lives;
            if (this.player.lives <= 0) {

                this.emitter.x = player.x;
                this.emitter.y = player.y;
                this.emitter.start(true,800,null,15);
                backgroundMusic.pause();
                player.kill();

                game.global.lastScore = score;
            
                game.time.events.add(500,function(){
                    if (score > game.global.highestScore){

                        var playerName = prompt('Well done! you break the record.\n Please enter your name:');
                         firebase.database().ref().set({
                             score: score,
                             name: playerName
                        });
                        game.global.highestScore = score;
                        game.global.name = playerName;
                     }

                    game.state.start('gameover');
                },this);
            }
            else {
                this.emitter.x = object.x;
                this.emitter.y = object.y;
                this.emitter.start(true,800,null,15);
             }
        }
        else {
            this.emitter.x = object.x;
            this.emitter.y = object.y;
            this.emitter.start(true,800,null,15);
        
            shieldOn = false;
        }
    },
    gamePauseDetect: function(){
        if (this.zxp.p.isDown){
            pauseLabel = game.add.text(game.width/2, game.height/2,
            'Press any key to continue.', { font: '30px Arial', fill: '#ffffff' });
            pauseLabel.anchor.setTo(0.5,0.5);

            game.paused = true;
            game.input.keyboard.onDownCallback = function(){
                pauseLabel.destroy();
                game.paused=false;
            };
        }
    },
    ultKill: function(){
        
        this.enemies.forEach(function(enemy){
            if(enemy.alive ){
                if (Phaser.Rectangle.intersects(this.explosionPic.getBounds(),enemy.getBounds())){
                    enemy.kill();
                    
                    score += 100;
                    this.scoreLabel.text = 'score: ' + score;
                }
            }
        },this);

        this.Ebullets.forEach(function(bullet){
            if(bullet.alive ){
                if (Phaser.Rectangle.intersects(this.explosionPic.getBounds(),bullet.getBounds())){
                    bullet.kill();
                }
            }
        },this);
    }

};
