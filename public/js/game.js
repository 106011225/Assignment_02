var game = new Phaser.Game(500,800,Phaser.AUTO,'canvas');

game.global = {highestScore: 0,name:'',  lastScore:0, gameWidth:500, gameHeight:800, volume:5, difficulty:0};

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('gameover', gameoverState);

game.state.start('boot');

